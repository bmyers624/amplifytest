import React, { useEffect, useState } from 'react'

import { Auth } from 'aws-amplify'
import { Row, Col, Button, Spinner, Form, Alert } from 'react-bootstrap'
import { Link, Redirect } from "react-router-dom"

function RegisterPage(props) {

  const [isLoading, setIsLoading] = useState(false)
  const [formEmail, setFormEmail] = useState("")
  const [formPhoneNumber, setFormPhoneNumber] = useState("")
  const [formPassword, setFormPassword] = useState("")
  const [formConfirmationCode, setFormConfirmationCode] = useState("")
  const [errorMessage, setErrorMessage] = useState("")

  const [signupUser, setSignupUser] = useState(null)

  async function signUp() {
    setIsLoading(true)

    // TODO Confirm signup
    // The Auth.signUp promise returns a data object of type ISignUpResult with a CognitoUser.
    //  user: CognitoUser
    //  userConfirmed: boolean
    //  userSub: string
    Auth.signUp({
      username: formEmail,
      password: formPassword,
      attributes: {
        email: formEmail,
        phone_number: formPhoneNumber,
      }
    }).then((user) => console.log("Registered user: " + user))
      .catch((error) => setErrorMessage(error.message || error.log))
      .finally(() => setIsLoading(false))
  }

  const handleSubmit = (event) => {
    console.log("Handle register submit!")

    event.preventDefault() // Validate for example email format

    if (!isLoading) {
      signUp()
    }
  }

  if (signupUser) {
    return (<Redirect to={"/confirm/" + formEmail } />)
  }

  return (
      <div className="container-fluid p-4 bg-light">
        <Row>
          <Col sm={6} className="m-auto p-4 border bg-white">
            <h4>Register</h4>
            {errorMessage && <Alert variant="danger">{errorMessage}</Alert>}
            <Form onSubmit={handleSubmit}>

              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" required
                  defaultValue={formEmail}
                  onChange={(event) => setFormEmail(event.target.value)} />
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" required minLength={6}
                  onChange={(event) => setFormPassword(event.target.value)} />
              </Form.Group>

              {/* TODO Verify that passwords match */}
              <Form.Group controlId="formBasicPasswordRepeat">
                <Form.Label>Repeat Password</Form.Label>
                <Form.Control type="password" placeholder="Repeat Password" required />
              </Form.Group>

              <Form.Group controlId="formBasicPhoneNumber">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control type="tel" placeholder="+43 - ..." required
                  defaultValue={formPhoneNumber}
                  onChange={(event) => setFormPhoneNumber(event.target.value)} />
              </Form.Group>

              <Link to="/login">I already have an account</Link>

              {isLoading && <Spinner animation="border" className="d-flex" variant="primary" size="sm"><span className="sr-only">Loading...</span></Spinner>}

              <Button variant="primary" className="w-100 mt-2"
                disabled={isLoading}
                type="submit">Create an account</Button>

            </Form>
          </Col>
        </Row>
      </div>
    )
}

export default RegisterPage