import React, { useEffect, useState } from 'react'

import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom"
import { Spinner } from 'react-bootstrap'

import { Auth } from 'aws-amplify'

import HomePage from './HomePage'
import LoginPage from './LoginPage'
import RegisterPage from './RegisterPage'
import LogoutButton from './LogoutButton'
import ConfirmPage from './ConfirmPage'

import "bootstrap/dist/css/bootstrap.css"

// Routing example: https://ui.dev/react-router-v4-protected-routes-authentication

function App() {

  const [user, setUser] = useState({ email: "" })
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    setIsLoading(true)

    Auth.currentAuthenticatedUser()
      .then((user) => setUser({ email: user.attributes.email }))
      .catch(() => setUser({ email: "" }))
      .finally(() => setIsLoading(false))
  }, [])

  if (isLoading) {
    return (<Spinner animation="border" variant="primary" size="sm"><span className="sr-only">Loading...</span></Spinner>)
  }

  return (
    <>
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          {/* TODO propagate user trough hooks */}
          {user.email ? <HomePage user={user} /> : <Redirect to="/login" /> }
        </Route>
        <Route path="/login">
          {user.email ? <Redirect to="/" /> : <LoginPage setUser={setUser} /> }
        </Route>
        <Route path="/register">
          {user.email ? <Redirect to="/" /> : <RegisterPage setUser={setUser} />}
        </Route>
        <Route path="/confirm/:email">
          <ConfirmPage />
        </Route>
      </Switch>
    </BrowserRouter>

    {user.email && <LogoutButton setUser={setUser} /> }
    </>
  )
}

export default App