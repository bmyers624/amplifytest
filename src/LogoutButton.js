import React, { useEffect, useState } from 'react'

import { Auth } from 'aws-amplify'
import { Button } from 'react-bootstrap'

function LogoutButton(props) {

  const [isLoading, setIsLoading] = useState(false)

  async function signOut() {
    setIsLoading(true)

    Auth.signOut()
    .then(() => props.setUser({ email: "" }))
    .finally(() => setIsLoading(false))
  }

  return (
    <div>
      <Button variant="danger" onClick={() => signOut()}>Sign me out</Button>
    </div>
  )
}

export default LogoutButton